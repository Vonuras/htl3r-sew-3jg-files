## 1 Zeichenstatistik: Schreibe die Methode
```
Map<Character, Integer> zeichenStatistik(String s) { .. } ,
```
die eine Map zurückliefert, deren Keys die Zeichen aus dem String s sind und als Value jeweils gespeichert ist,
wie oft das jeweilige Zeichen vorkommt.
```
zeichenStatistik("Hallo Welt wie geht es heute") -> { =5, a=1, e=6, g=1, h=2, H=1, i=1, l=3, o=1, s=1, t=3, u=1, w=1, W=1}
```
## 2 UE03 (2021/22) – Klasse Birthday-Datamining
Überarbeite die einzelnen Funktionen in der Klasse BirthDayDatamining aus [ADV - Collection/Map](https://my.skilldisplay.eu/en/skillset/624) so, dass möglichst viele
Methoden mit Hilfe von Streams umgesetzt werden (dies betrifft vor allem die ersten 5 Teilaufgaben .. )

## 3 Winner
- Löse alle Aufgaben mit Streams, verwende die Liste **tdfWinners** aus der Klasse [Winner.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Winner.java).
- Teste jede Aufgabe in der main-Methode mit einer Ausgabe wie in der Musterausgabe.
Aufgaben
1. Ermittle eine Liste mit den Namen aller Gewinner mit Touren, die kleiner als 3500km sind.
2. Ermittle eine Liste mit den Namen aller Gewinner mit Touren, die größer als 3500km sind.
3. Ermittle eine Winner-Object-Liste mit den ersten beiden Gewinnern mit Touren, die kleiner als 3500km sind.
4. Ermittle eine Liste mit allen Namen mit den ersten beiden Gewinnern mit Touren, die kleiner als 3500km sind.
5. Ermittle eine Liste mit allen Namen aller Gewinner von mindestens einer Tour. Kein Name darf doppelt vorkommen.
6. Ermittle die Anzahl aller Gewinner von mindestens einer Tour. (Zähle Gewinner nicht doppelt, wenn sie mehrere Touren gewonnen haben.)
7. Ermittle eine Winner-Object-Liste, die alle bis auf die ersten beiden Elemente von **tdfWinners** enthält.
8. Ermittle eine String-Liste mit der Info: **Jahr - Gewinner**, …
9. Ermittle eine Integer-Liste mit allen Längen der Namen.
10. Ermittle irgendein (das erste) Sieger-Jahr von **Wiggins**.
11. Ermittle den Gewinner des Jahres 2014.
12. Ermittle die Gesamtlänge aller Touren.
13. Ermittle die Länge der kürzesten Tour.
14. Ermittle die Länge der längsten Tour.
15. Ermittle den Gewinner mit der höchsten Durchschnitts-Geschwindigkeit.
16. Ermittle die höchste Durchschnitts-Geschwindigkeit.
17. Erzeuge einen String, der für jedes Element von **tdfWinners** das Team enthält. Die einzelnen Teams sollen mit einem Beistrich von einander getrennt sein.
18. Erzeuge eine Map, die für jede Nationaltität eine Liste mit ihren Winner-Objekten enthält.
19. Erzeuge eine Map, die für jede Nationaltität die Anzahl ihrer Siege enthält.

Musterausgabe
```
1. Winners of Tours Less than 3500km - [Alberto Contador, Cadel Evans, Bradley Wiggins, Chris Froome, Chris F
2. Winners of Tours Greater than 3500km - [Óscar Pereiro, Alberto Contador, Carlos Sastre, Andy Schleck, Vin
3. winnerObjectsOfToursLessThan3500kmLimit2 [Alberto Contador, Cadel Evans]
4. firstTwoWinnersOfToursLessThan3500km - [Alberto Contador, Cadel Evans]
5. distinctTDFWinners - [Óscar Pereiro, Alberto Contador, Carlos Sastre, Andy Schleck, Cadel Evans, Bradley
6. numberOfDistinceWinners - 8
7. skipEveryOtherTDFWinner - [Carlos Sastre, Alberto Contador, Andy Schleck, Cadel Evans, Bradley Wiggins,
8. mapWinnerYearNamesToList [2006 - Óscar Pereiro, 2007 - Alberto Contador, 2008 - Carlos Sastre, 2009 - Alb
9. mapWinnerNameLengthToList [13, 16, 13, 16, 12, 11, 15, 12, 15, 12, 12]
10. Wiggins wins - 2012
11. winnerYear2014 - Vincenzo Nibali
12. totalDistance - 38767
13. shortestDistance - 3360
14. longestDistance - 3661
15. fastestTDF winner - Óscar Pereiro
16. fastestTDF aveSpeed - 41.0
17. allTDFWinnersTeams Caisse d'Epargne–Illes Balears, Discovery Channel, Team CSC, Astana, Team Saxo Bank,
18. winnersByNationality - {Great Britain=[Bradley Wiggins, Chris Froome, Chris Froome, Chris Froome], Luxem
19. winsByNationalityCounting - {Great Britain=4, Luxembourg=1, Italy=1, Australia=1, Spain=4}
```


## 4 Log Analyse
- Schreibe eine Logfile-Auswertung. Schreibe dazu die Methode **printLogStatistik**, die für eine beliebige Datei (verwende [access_log.zip](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/access_log.zip)) die folgenden Statistiken mit Hilfe von einzelnen Methoden ermittelt:
  - **getMostCommonIp**: Welcher Rechner (IP-Adresse) surft am meisten?
  - **getMostCommonPage**: Welche Seite wird am öftesten aufgerufen?
  - **countDownloadBytes**: wieviele Bytes wurden übertragen? (verwende long)


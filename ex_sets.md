## Aufgabe 1: Objekte dieser Klasse sollen als Elemente von Sets verwendet werden:
```
public class Person {
  public String vorname;
  public String famname;
}
```
Erweitere diese Klasse so, dass:

- es in einem TreeSet keine Personen mit gleichem Vor- und Familiennamen geben kann, unabhängig von Groß-/Kleinschreibung und von führenden/folgenden Leerzeichen
- sie in TreeSets zuerst nach Familiennamen und (bei gleichem Familiennamen) nach Vornamen sortiert wird

## Aufgabe 2: Schreibe eine Klasse Punkt, die einen Punkt in der Ebene darstellt.

- Ein TreeSet soll nicht mehrere Punkt-Objekte mit denselben Koordinaten enthalten können.
- In TreeSets sollen Punkt-Objekte nach ihrem Abstand vom Koordinatenursprung sortiert sein. (Punkte mit gleichem Abstand, aber unterschiedlichen Koordinaten (z.B.: (1|1) , (-1|-1)), dürfen nicht als Duplikate interpretiert werden – überlege Dir eine sinnvolle Sortierung)

## Aufgabe 3: Schreibe die Klasse Buch, die für ein Buch den Titel, den Autor und den Preis in Cent speichert.

- Zwei Bücher sollen als gleich gelten, wenn Titel und Autor gleich sind (für das Hashset).
- Erzeuge fünf Bücher deiner Wahl, speichere sie in einem HashSet und gib dieses aus.
- Gib die Bücher sortiert nach Preis, Titel und Autor aus. ( verwende dazu ein TreeSet und implementiere Comparable in der Klasse Buch)
- Gib die Bücher auch noch sortiert nach Titel, Autor und Preis aus. (weiteres TreeSet + Comparator)

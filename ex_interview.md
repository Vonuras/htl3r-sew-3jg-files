# Interview

Mach dir einen Termin aus, an dem du ein Interview führst.

In diesem Interview wirst du theoretische Fragen bekommen und eventuell ein Mini-Coding-Beispiel lösen müssen (Je nach Rolle).

Das Interview kann für eine der folgenden Jobs geführt werden:

- Software Developer (Backend, Frontend oder Full-Stack)
- Software Tester

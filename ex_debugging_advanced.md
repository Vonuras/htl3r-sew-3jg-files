## Problem 1
Jemand hat ein Spiel programmiert, und benötigt jetzt deine Hilfe.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[Game.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Game.java)

## Problem 2
Jemand hat ein kleines Programm geschrieben um Filme zu sortieren und es will einfach nicht funktionieren.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[Movies.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Movies.java)

## Problem 3
Ein Restaurant möchte mit einem einfachen Programm zeigen, welche Pizza wieviel kostet.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[Pizza.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Pizza.java)





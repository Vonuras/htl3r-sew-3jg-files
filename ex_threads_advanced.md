# Threads - Pferderennen

Verwende die folgendes File:
[Pferderennen_Template.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Pferderennen_Template.java)

Erstelle eine GUI-Anwendung zur Simulation eines Pferderennens. Jedes Pferd wird in einem eigenen
Thread verwaltet und die zurückgelegte Strecke durch eine Progressbar dargestellt.

![Pferderennen_Bild1.png](data_files/Pferderennen_Bild1.png "Pferderennen_Bild1.png")

Mit dem "+" und "–" Knopf kann die Anzahl der "Pferde" (Progressbars) verändert werden. Nach dem
Drücken des "Start-Knopfes" laufen die einzelnen Threads los:
- zufälliger Schritt vorwärts
- zufällige Pause

![Pferderennen_Bild2.png](data_files/Pferderennen_Bild2.png "Pferderennen_Bild2.png")

Beim Erreichen des Ziels wird der Text "Horse/Thread-# ist im Ziel" in das Ergebnisfeld ausgegeben.

![Pferderennen_Bild3.png](data_files/Pferderennen_Bild3.png "Pferderennen_Bild3.png")

Wichtig:
- definiere Konstanten für die Länge der Pause, die maximale Schrittweite, etc.
- überlege sinnvolle Datenstrukturen zum Verwalten der vielen Progressbars und Threads.
- Sonderfälle wie: "+", "-" nach Start ...

Hinweise:
- die GUI darf schöner sein
-Template: eine *BorderPane*
  - oben ein *Label*
  - unten eine *TextArea*
  - links: eine *VBox* mit zwei *Button*s
  - rechts: ein *Button*
  - mittig: eine *VBox* mit *Progressbar*s
    - maximale Größe/Länge: *progressbar.setMaxWidth(Double.MAX_VALUE)*

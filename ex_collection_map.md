## Aufgabe 1: Schreibe die Methode

```
Map<Character, Double> zeichenStatistik(String text) { … }
```
Diese Methode soll im Ergebnis zu jedem char aus dem text die relative Häufigkeit des Vorkommens
(bezogen auf die Textlänge) liefern, d.h. eine Zahl zwischen 0 und 1. Zwischen Groß- und Kleinschreibung soll
nicht unterschieden werden.

## Aufgabe 2: Schreibe die Methode

```
Map<Character, Double> zeichenStatistik(Path datei) { … }
```
Diese Methode soll die Textdatei datei (in der Default-Zeichencodierung) so untersuchen wie für Aufgabe 1.
beschrieben. Bei IOExceptions soll die Methode eine leere Map liefern.

## Aufgabe 3: Klasse Birthday

Schreibe die Klasse Birthday , die einen Geburtstag (= Datum) speichert.

- Sie soll einen Konstruktor haben, der einen String im Format tt.mm.yyyy übernimmt.
  - Beispiel 22.05.1966
  - Der Konstruktor muss das Datum nicht auf Gültigkeit überprüfen (44.05.1966 wird akzeptiert).
- Die Klasse soll Getter für den Tag, das Monat sowie das Jahr haben.
- Sie soll keine Setter haben.
- Sie soll das Interface Comparable sinnvoll implementieren.
  - Das ist notwendig, um Geburtstage in ein TreeSet einfügen zu können, bzw. zu sortieren.


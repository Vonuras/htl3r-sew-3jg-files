# Progress Route

<table>
<thead align="center">
<th>Beginner</th>
<th>Intermediate</th>
<th>Expert</th>
<th>Outside</th>
</thead>
<tbody align="center">
<tr>
<td colspan=3> [Refresher](https://my.skilldisplay.eu/en/skillset/618) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_refresher.md)</td>
<td>[Interview](https://my.skilldisplay.eu/en/skillset/679) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_interview.md)</td>
</tr>
<tr>
<td colspan=3> [Collection/IO](https://my.skilldisplay.eu/en/skillset/622) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_collection_io.md)</td>
<td>[ADV_Debugging_Additional](https://my.skilldisplay.eu/en/skillset/672) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_debugging_advanced_additional.md)</td>
</tr>
<tr>
<td colspan=3> [Debugging](https://my.skilldisplay.eu/en/skillset/647) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_debugging_basics.md)</td>
<td> [ADV_JavaFX_Wiki](https://my.skilldisplay.eu/en/skillset/677) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_javafx_html_wiki.md)</td>
</tr>
<tr>
<td>v</td>
<td colspan=2> 
 [ADV_Debugging](https://my.skilldisplay.eu/en/skillset/645) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_debugging_advanced.md)</td>
<td>[ADV_Project](https://my.skilldisplay.eu/en/skillset/678) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_project_advanced.md)</td>
</tr>
<tr>
<td colspan=3> [Sets](https://my.skilldisplay.eu/en/skillset/620) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_sets.md)</td>
<td rowspan=20></td>
</tr>
<tr>
<td colspan=3> [Collections/Map](https://my.skilldisplay.eu/en/skillset/627) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_collection_map.md)</td>
</tr>
<tr>
<td>v</td>
<td colspan=2>
[ADV_Collections/Map](https://my.skilldisplay.eu/en/skillset/624) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_collection_map_advanced.md)</td>
</tr>
<tr>
<td colspan=3> [Collections/Map/Set](https://my.skilldisplay.eu/en/skillset/631) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_collection_map_set.md)</td>
</tr>
<tr>
<td colspan=3> [Lambdas](https://my.skilldisplay.eu/en/skillset/615) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_lambdas.md)</td>
</tr>
<tr>
<td colspan=3> [Streams/Basics](https://my.skilldisplay.eu/en/skillset/619) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_streams_basics.md)</td>
</tr>
<tr>
<td colspan=3> [Streams/Extended](https://my.skilldisplay.eu/en/skillset/635) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_streams_extended.md)</td>
</tr>
<tr>
<td colspan=3> [Junit](https://my.skilldisplay.eu/en/skillset/673) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_junit.md)</td>
</tr>
<tr>
<td colspan=3> [Regex](https://my.skilldisplay.eu/en/skillset/629) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_regex_basics.md)</td>
</tr>
<tr>
<td colspan=3> [Java/Regex](https://my.skilldisplay.eu/en/skillset/630) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_java_regex_basics.md)</td>
</tr>
<tr>
<td>v</td>
<td colspan=2>
[ADV_Java/Regex](https://my.skilldisplay.eu/en/skillset/617) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_java_regex_advanced.md)</td>
</tr>
<tr>
<td colspan=2">v</td>
<td>
[ADV_Streams/File/Morse](https://my.skilldisplay.eu/en/skillset/637) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_streams_advanced_files_and_morse.md)
</td>
</tr>
<tr>
<td colspan=2">v</td>
<td>
[ADV_Streams/Logfile](https://my.skilldisplay.eu/en/skillset/636) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_streams_advanced_logfile.md)</td>
</tr>
<tr>
<td colspan=3> [Recursion/Bascis](https://my.skilldisplay.eu/en/skillset/632) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_recursion_basics.md)</td>
</tr>
<tr>
<td colspan=3> [Recursion/Extended](https://my.skilldisplay.eu/en/skillset/633) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_recursion_extended.md)</td>
</tr>
<tr>
<td colspan=2>v</td>
<td>
[ADV_Recursion](https://my.skilldisplay.eu/en/skillset/646) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_recursion_advanced.md)</td>
</tr>
<tr>
<td colspan=2>v</td>
<td> [ADV_Threads](https://my.skilldisplay.eu/en/skillset/674) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_threads_advanced.md)</td>
</tr>
<tr>
<td colspan=3> [Server/Mathe](https://my.skilldisplay.eu/en/skillset/675) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_mathe_server.md)</td>
</tr>
<tr>
<td>v</td>
<td colspan=2> [ADV_Server/Chat](https://my.skilldisplay.eu/en/skillset/676) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_chat_server_advanced.md)</td>
</tr>
<tr>
<td>v</td>
<td colspan=2> [ADV_Definitely_not_a_PLF](https://my.skilldisplay.eu/en/skillset/680) - [Angabe](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/ex_plf_advanced.md)</td>
</tr>
</tbody>
</table>


# Definitely not a PLF


## I) Definitely not PLF1

### Aufgabe 1: Collections - Set (7 Punkte)

#### a) (4 Punkte)

Erweitere die Klasse MyDate aus der Angabe so, dass Objekte dieser Klasse sowohl in einem TreeSet<MyDate> als auch in einem HashSet<MyDate> verwaltet werden können.
```
public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
```
Die Sortierung der Objekte im TreeSet soll - jeweils aufsteigend - nach folgenden Reihenfolge erfolgen:
Jahr -> Monat -> Tag
Zwei **MyDate**-Objekte sind gleich, wenn **Jahr**, **Monat** UND **Tag** gleich sind.

#### b) (3 Punkte)

Die Klasse **JLabel** (**javax.swing.JLabel**)  - eine Standard Java Klasse - kann, so wie sie implementiert wurde, **NICHT** in **TreeSets** gespeichert werden. 
- **Implementiere** und **verwende** ein geeignetes Werkzeug, um dieses Problem zu lösen. (Von JLabel abzuleiten ist hier als Lösung NICHT gewünscht!) 

**JLabel** Objekte enthalten (im simplen Fall) **Strings**, die in GUIs dargestellt werden (z.B.: um Eingabefelder zu beschriften).
- JLabel Objekte im **TreeSet** sollen nach der Länge diser Texte sortiert werden (längere Beschriftungen zuerst). 
- Zwei **JLabel** Objekte sind dann gleich, wenn die beiden Texte ohne Berücksichtigung der Groß-/Kleinschreibung gleich sind. 
  -Die Methode nameDesLabels**.getText()** liefert den im Label gespeicherten Text.

Ein Testfall: (achte darauf, die RICHTIGE JLabel-Klasse zu importieren -> javax.swing.JLabel !!

```
JLabel lab1 = new JLabel("kurzer Text");
JLabel lab2 = new JLabel("KURZER TEXT");
JLabel lab3 = new JLabel("Ein laaaanger Text");

System.out.println(lab3.getText());

TreeSet<JLabel> allLabels = new TreeSet<>();

allLabels.add(lab1);
allLabels.add(lab2);       
allLabels.add(lab3);

System.out.println(allLabels);
```

### Aufgabe 2: Collections - Map + Regex (9 Punkte)

Die beigefügte Datei [Meisterschaft2.txt](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Meisterschaft2.txt) enthält alle Spielergebnisse einer vergangenen Fussballsaison der österreichischen Regionalliga Ost. 

Jeweils zwei Zeilen in der Datei enthalten zuerst (1. Zeile) den Spieltag (=Runde), danach (folgende Zeile) alle Spiele der Runde mit ihren Ergebnissen. 

Schreibe die Methode 
```
Map<... , ...> createGoalStatistic(Path file) { .. }  
```
mit folgender Aufgabe:
- (1) Die Datei ist einzulesen und zeilenweise abzuarbeiten
- (3,5) Mit Hilfe eines passenden regulären Ausdrucks UND sinnvoller Verwendung der RegEx-typischen Werkzeuge (**Pattern**, **Matcher**) sollen alle Spielergebnisse (Endergebnisse) der gesamten Meisterschaft "lokalisiert" werden. 
  - In einer Zeile stehen mehrere Ergebnisse!
  - Betrachte den Aufbau der Datei: die ersten Spielergebnisse sind also dann: **1:1** , **2:2**, **1:2**, ...
- (1,5) Durch sinnvolle Definition und Einsatz von Gruppen (**s.split()** führt als Lösung zu Punkteverlust)  im regulären Ausdruck soll für jedes Spielergebnis die Gesamtanzahl der im Spiel erzielten Tore ermittelt werden. Im obigen Beispiel wäre das : 2, 4, 3, ...
- (1) Über diese Gesamttoranzahl soll eine Statistik gebildet werden: Wie oft wurden wie viele Tore in einem Spiel erzielt.
- (2) Verwende zum Ermitteln dieser Statistik eine **Map<..., ...>** , die von der Methode als Ergebnis zurückgeliefert wird.

### Auswertung:

Bsp 1:
- a)
  - Implements Comparable: 	**0,5**
  - compareTo()			**2**
  - hashcode()			**1**
  - equals()			**0,5**
- b)
  - Comparator (def und verw) 	**1,5**
  - compare()			**1,5**

Bsp 2:
- Datei lesen, jede zweite Zeile	**1**
- RegEx funktioniert		**2**
- Pattern und Matcher
korrekt angewendet (while ..)	**1,5**
- Gruppen für die Tore		**1,5** (kein split(„:“)
 Gesamttore ermittelt		**1**
- In Map gezählt + Rückgabe	**2**

## II) Definitely not PLF2

### Aufgabe 1: (Die Speisekarte) (10 Punkte)

Die Klasse [Speise.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Speise.java) ist bereits verfügbar und erzeugt eine statische **List<Speise>** von Speise-
Objekten, auf die in den folgenden Teilaufgaben zugegriffen werden soll.

#### Teilaufgaben:
Schreibe die folgenden Methoden, die mit Hilfe von Stream-Operationen die jeweils
geforderte Aufgabe erfüllen (Achte besonders auf die geforderten Rückgabetypen !!)
- Eine Methode **getAllNames() {..}**, die eine alphabetisch sortierte Liste mit den Namen aller Speisen zurückliefert 
→[Beef Tartare, Chili sin carne mit geraeuchertem Tofu, Gebackene Apfel...] **1P**
- Eine Methode **getCheapOnes() {..}**, die eine Liste mit allen Speise-Objekten zurückliefert, die billiger als 7 Euro sind
→[Fritattensuppe, Gulaschsuppe, Knoblauchcremesuppe,  ...] **1P**
- Eine Methode **getMostExpensive() {..}**, die die beiden teuersten Speisen als EIN **String** im Format **"Speise1 (Preis) , Speise2 (Preis)"** zurückliefert
→Zwiebelrostbraten (15.5) , Zander vom Grill (14.9) **1P**
- Eine Methode **getSaveOnes(String allergen) {..}**, die eine **String**-Liste von Speisen zurückliefert, die das als Parameter übergebene Allergen (z.B.: “L“) nicht enthalten (nur EIN Zeichen wird übergeben)
(Format : Speise1 (enthaltene Allergene))
→[Beef Tartare (ACM), Weinbergschnecken (AGR), Rindercarpaccio mit Fladenbrot (ACGNP), ...] **1P**
- Eine Methode **getAverageMain() {..}**, die den Durchschnittspreis aller Hauptspeisen zurückliefert
→10.11 **1P**
- Eine Methode **getAsMap() {..}**, die eine **Map** zurückliefert, in der als Value jeweils eine Liste mit Speisen-Objekten zu den Keys Vorpeise, Suppe, Hauptspeise .. gespeichert ist.
→{Vorspeise=[Beef Tartare, Weinbergschnecken, Rindercarpaccio mit Fladenbrot], Nachspeise=[Schokoladepalatschinken ...] **1P**
- Eine Methode **getCaloriesSum() {..}**, die eine **Map** zurückliefert in der als Value zu den Keys Vorpeise, Suppe, Hauptspeise .. jeweils die Summe der Kalorien der entsprechenden Speisen gespeichert ist
→{Vorspeise=1190, Nachspeise=1339, Hauptspeise=5414, Suppe=1172} **1,5P**
- Eine Methode **getVegetarianMeals() {..}**, die eine **String**-Liste mit den Namen aller vegetarischen Speisen, absteigend nach ihren Kalorien sortiert, im Format "Speise : Kalorien" zurückliefert.
→[Kaesespaetzle mit gemischtem Salat: 1025, Lauchpancakes mit Kaesesauce: 948,
Schokomousse-Trilogie: 457, ...] **1,5P**
- Eine Methode **getTheFattener() {..}**, die das Speisen-Objekt mit den meisten Kalorien zurückliefert.
→Kaesespaetzle mit gemischtem Salat **1P**


### Aufgabe 2a: (zum Aufwärmen) (2,5 Punkte)

Schreibe eine **rekursive** Methode
```
int digitSumSpecial(String s) {..},
```
welche die Ziffernsumme **aller Ziffern** im **String s** liefert.

```
digitSumSpecial("1a32b35c") → 14
digitSumSpecial("Hallo Welt") → 0
```

### Aufgabe 2a: (sportlich ...) (3,5 Punkte)

Wenn man von einem Fussballspiel (o.Ä.) nur das Endergebnis kennt (z.B.: **2:1**) wäre es (zumindest
für Mathematiker) interessant, herauszufinden, wie der Spielverlauf (also die Torfolge) war und wie
viele unterschiedliche Verläufe das Spiel genommen haben könnte.
Bei einem Endergebnis von **2:1** wären folgende Spielverläufe möglich:
```
1:0 -> 2:0 -> 2:1 oder 1:0 -> 1:1 -> 2:1 oder 0:1 -> 1:1 -> 2:1
➔ also 3 Möglichkeiten
```
Schreibe eine **rekursive** Methode, mit der die Anzahl der möglichen Spielverläufe für ein gegebenes
Endergebnis ermittelt werden kann.
Teste mit :
- 0:0 → 1
- 4:0 → 1
- 2:1 → 3
- 2:5 → 21

Lösungsansatz: Ausgehend von jeweils 0 Toren (0:0) wird die Anzahl der Tore jeweils um 1 erhöht, bis das Endresultat
erreicht wird ...


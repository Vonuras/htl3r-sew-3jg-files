## 1 Fibonacci- Zahlen
Die Fibonacci-Folge ist die unendliche Folge von natürlichen Zahlen, die mit zweimal der Zahl 1 beginnt. Im Anschluss ergibt jeweils die Summe zweier aufeinanderfolgender Zahlen die unmittelbar danach folgende Zahl (siehe https://de.wikipedia.org/wiki/Fibonacci-Folge)
Es gilt also:
```
f(n) = f(n-1) + f(n-2)
```
Die ersten Elemente der Folge sind demnach:
```
1 1 2 3 5 8 13 21 …
```
Schreibe eine rekursive Methode
  ```
  int fibonacci(int n) { .. } ,
  ```
  die das n-te Element der Fibonacci-Folge ermittelt und zurückliefert
  fibonacci(1) → 1 , fibonacci(2) → 1 , fibonacci(3) → 2 , ….
Schreibe eine Methode
  ```
  void printFibonacciFolge(int anz) { .. },
  ```
  die mit Hilfe der obigen Funktion **fibonacci(..)** eine Folge von Fibonacci-Zahlen mit anz Elementen ausgibt (das Beispiel weiter oben wird von **printFibonacciFolge(8)** erzeugt ..)
## 2 Rekursion mit Strings
Schreibe eine rekursive Methode
  ```
  int zaehleSelbstlaute(String s) { .. } ,
  ```
  die zählt und zurückliefert, wie viele Selbstlaute im String s enthalten sind (Groß-/Kleinschreibung soll ignoriert werden).
  [rekursiver Ansatz: Teilergebnis = 1 Zeichen + Reststring analog weiter bearbeitet ……]
## 3 Projekt Euler: Coin sums
In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
```
1p , 2p , 5p , 10p , 20p , 50p , £1 (100p) and £2(200p).
```
It is possible to make £2 in the following way:
```
1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
```
How many different ways can £2 be made using any number of coins?


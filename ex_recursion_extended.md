## 1 Zahl hoch
Viele (9) Multiplikationen
  ```
  2 ^ 10 = 2 * 2 * ...
  ```
Oder: durch geschicktes Zusammenfassen - 4 Multiplikationen
  ```
  2 ^ 10 = 2 ^ 5 * 2 ^ 5
           2 ^ 5 = 2 * 2 ^ 2 * 2 ^ 2
                       2 ^ 2 = 2 ^ 1 * 2 ^ 1
                               2 ^ 1 = 2
  ```
Schreibe die Methode **zahlHoch(int x, int n)** die (rekursiv) nach der zweiten Methode rechnet
  Löse: wenn man im Jahr 0 einen Euro mit 1 % verzinst angelegt hätte, wieviel wäre es heute ?




## 2 Maximale Summe
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
  ```
     3
    7 4
   2 4 6
  8 5 9 3
  ```
  That is, 3 + 7 + 4 + 9 = 23.
Find the maximum total from top to bottom of the triangle below:
```
                            75
                          95  64
                        17  47  82
                      18  35  87  10
                    20  04  82  47  65
                  19  01  23  75  03  34
                88  02  77  73  07  63  67
              99  65  04  28  06  16  70  92
            41  41  26  56  83  40  80  70  33
          41  48  72  33  47  32  37  16  94  29
        53  71  44  65  25  43  91  52  97  51  14
      70  11  33  28  77  73  17  78  39  68  17  57
    91  71  52  38  17  14  91  43  58  50  27  29  48
  63  66  04  68  89  53  67  30  73  16  69  87  40  31
04  62  98  27  23  09  70  98  73  93  38  53  60  04  23
```
NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route.
- Find the maximum total from top to bottom of the triangle in file [numberTriangle.txt](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/numberTriangle.txt)
- (can NOT be solved using backtracking)

## 3 Projekt Euler: Longest Collatz sequence
The following iterative sequence is defined for the set of positive integers:
```
n -> n/2 (n is even), n -> 3n + 1 (n is odd)
```
Using the rule above and starting with 13 we generate the following sequence:
```
13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
```
It can be seen that this sequence (starting at 13 and finishing on 1) contains 10 terms. Although it has not been proven yet (Collatz Problem), it is thought that all starting numbers finish on 1.
Which starting number, under one million (1e6), produces the longest chain ?
**NOTE: Once the chain starts, the terms are allowed to go above one million.**

## 4 Sierpinski-Dreieck
Zeichne das [Sierpinski-Dreieck](https://de.wikipedia.org/wiki/Sierpinski-Dreieck).
- Gehe von der Java-Datei [DemoSimpleDrawing.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/DemoSimpleDrawing.java) aus.
- _**Javafx muss installiert und in der IDE eingebunden sein!**_
- Die eigentliche rekursive Zeichen-Methode soll **void drawSierpinsikiTriangle** heißen und die folgenden Parameter haben:
  - GraphicsContext gc: die Zeichenfläche
  - int size: Abbruchkriterium (Anzahl der Pixel der kürzesten Kantenlänge, die noch gezeichnet werden soll).
  - Koordinaten für die 3 Eckpunkte (double) des großen Dreiecks.
- Das große Dreieck besteht aus drei kleineren Dreiecken – diese kann man durch einen rekursiven Aufruf zeichnen.

Beispiel:
- Die Seitenlänge des großen Dreiecks soll 580 Pixel sein.
- Die kürzeste Seite, die noch gezeichnet wird, soll maximal 10 Pixel sein.

Hinweise:
- das Dreieck kann auch schief sein
- es hat schon jemand ein Klasse für Punkte geschrieben (Point2D, Teil von JavaFX)


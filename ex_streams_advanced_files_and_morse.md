**Die folgenden Beispiele sind mit Streams zu lösen**
## 1 größte Dateien 1
Schreibe die Methode
```
List<Path> biggestFilesA(Path dir, int n) { .. },
```
die im angegebenen Pfad (idealerweise ein Ordner) **dir** die größten **n** Dateien ermittelt und nach ihrer Größe sortiert zurückliefert (Größte zuerst).
Info:
```
Path path = ....;
File file = path.toFile();
System.out.println(file.is...);
//wandelt Path ein ein File um
```
## 2 größte Dateien 2
Schreibe die Methode
```
List<Path> biggestFilesB(Path dir, int n) { .. },
```
die im angegebenen Pfad **dir** und in allen darunter liegenden Verzeichnissen die größten **n** Dateien ermittelt und nach ihrer Größe sortiert zurückliefert (Größte zuerst).
## 3 ältere Dateien
Schreibe die Methode
```
List<Path> olderThan(Path dir, Path reference) { .. }.
```
Ist **reference** eine Datei, so soll die Methode alle Dateien unmittelbar aus dem **dir** liefern, die älter sind als **reference**. Ist **reference** ein Verzeichnis, so soll die Methode alle älteren Verzeichnisse liefern.
Hilfreiche Links:
https://mkyong.com/java/how-to-get-the-file-creation-date-in-java/
https://www.oreilly.com/content/handling-checked-exceptions-in-java-streams/
## 4 Morsecode
Schreibe die Methode **toMorseCode**, die einen beliebigen Text in Morsecode umwandelt und die Methode **morseToText**, die einen übergebenen Morsecode in Text umwandelt
**Die Methoden sollen alle Buchstaben, Umlaute und Ziffern beherrschen. Alle Sonderzeichen und Satzzeichen können ignoriert werden.**

Beispiel:
```
"Älle lieben Java!"
→ /.-.-/.-../.-.././/.-../.././-…/./-.//.---/.-/…-/.-///       -> toMorseCode
→ "älle lieben java"                                            -> morseToText
```

Hinweis: Es gibt keine Unterscheidung zwischen Groß- und Kleinbuchstaben. Zwischen den Buchstaben steht ein Schrägstrich, zwischen den Worten stehen zwei Schrägstriche [und zwischen den Sätzen stehen drei Schrägstriche → Herausforderung .

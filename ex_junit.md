# Junit Tests

Verwende die folgenden Files:
[Speise.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Speise.java)
[Speisekarte.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Speisekarte.java)

Ergänze die abgegebene Klasse Speisekarte durch JUnit-Tests.

Teste jede Methode der Klasse ausgiebig auf ALLE möglichen Fehler.

- wähle sinnvolle Namen für die Tests
- Rufe jede Methode der Speisekarte mehrmals auf
- teste (assert...) das Ergebnis

Ziel:
bei "Run xxxxxxxTest with Coverage" sollten alle Methoden und Zeilen abgedeckt sein.

Deine JUnit Testklasse sollte dann mit eingebauten Fehlern in der Speise, oder der Speisekarte Klasse alle Fehler finden.

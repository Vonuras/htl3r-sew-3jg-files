Für die folgenden Beispiel sollen vorgegebene Funktionen aus Collection (, ..)List, … mit Hilfe von Lambda-Ausdrücken implementiert werden.
Folgende Methoden werden u.A. benötigt:
```
.forEach(Consumer)
.replaceAll(UnaryOperator)
.removeIf(Predicate)
.sort(Comparator)
```

Verwende stehts Lambdas (keine inneren Funktionen)

## 1. Schreibe die Methode

```
void print(Collection<?> collection) {}
```

die jedes Element der collection auf der Konsole ausgibt.

## 2. Schreibe die Methode

```
Collection<String> hauptworte(Collection<String> worte) {}
```

die eine Collection liefert, die nur diejenigen worte enthält, die Hauptworte sind.

- Hautpworte stehen nur aus Buchstaben und beginnen mit einem Großbuchstaben.
- Verwende dazu die Collection-Methode **removeIf()** oder **filter()**
- Einzelne **worte** können auch Leerstrings oder null sein
Beispiel:

Für die Collection >null, "Hugo", "HTL", "HTL3R", "", "Haus-Boot", "SEW", "Softwareentwicklung"<
wird die Collection >"Hugo", "HTL", "SEW", "Softwareentwicklung"< zurückgegeben

## 3. Schreibe die Methode

```
List<Double> mult(List<Double> zahlen, double faktor) {}
```

Die Methode soll eine List<Double> liefern, die alle **zahlen** enthält, jedoch multipliziert mit dem **faktor**.
Verwende dazu die List-Methode replaceAll() oder mapToDouble()

## 4. Schreibe die Methode

```
List<Double> func(List<Double> zahlen, UnaryOperator<Double> op) {}
```

Die Methode soll eine List<Double> liefern, die alle zahlen enthält, wobei auf jede Zahl ein Operator op angewendet werden soll.
Verwende dazu die List-Methode replaceAll() oder mapToDouble()

## 5. Schreibe die Methode

```
List<String> numerisch(String ... elements) {}
```

die eine List liefert. Alle Elemente **die wie ganze Zahlen aussehen**, sollen aufsteigend nach dem Zahlenwert sortiert werden. Alle Elemente (Strings) sollen alphabetisch sortiert werden.

Beispiel:
Die Elemente "12", "Wappler", "8", "-5", "8", "Hugo", "-6", "-10", "Hugo", "-10", "7"
werden folgendermaßen sortiert: [-10, -10, -6, 7, 8, 8, 12, Hugo, Hugo, Wappler]

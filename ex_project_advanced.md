## Project Work

Such dir 1-2 KollegInnen und programmiert ein Projekt eurer Wahl über eine Laufzeit von 2-4 Wochen.

Das Projekt sollte in eine von 3 Kategorien fallen:
- etwas neues (eine kleine Anwendung, die jemandem helfen kann, oder eine Firma benötigt, Ferialpraxis Ideen ?)
- eine Erweiterung von etwas das es schon gibt (zB. ein open Source Projekt wie Signal)
- Bugfixing in einem Projekt, das es schon gibt (zB. ein open Source Projekt wie Signal)

WICHTIG:
Dieses Projekt muss nicht in Java geschrieben werden!

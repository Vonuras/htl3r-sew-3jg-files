# Mini Wiki

Verwende folgendes File:

[MiniWiki.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/MiniWiki.java)


![JavaFX_Wiki.png](data_files/JavaFX_Wiki.png "JavaFX_Wiki.png")

"The JavaFX WebView (javafx.scene.web.WebView) component is capable of showing web pages (HTML, CSS,
SVG, JavaScript) inside a JavaFX application. As such, the JavaFX WebView is a mini browser."
[Link zum Tutorial](http://tutorials.jenkov.com/javafx/webview.html)

Um Webview zu installieren:
- In IntelliJ gehe zu "Project Structure" (im "File" Kontext Menü)
  - Gehe zu "Libraries"
  - Gehe auf den "+" (Plus) Button
  - Suche mit "From Maven" nach "javafx-web" und nimm die höchste Version
  - "OK" Button
  - Gehe in den Settings auf "Apply" oder "OK"
- in deinem Projekt öffne die Datei "module-info.java"
  - ergänze die Zeile "    requires javafx.web;"

Wir wollen eine einfache App
entwickeln, die Wiki-Syntax in HTML
übersetzt.
- links die Eingabe
- rechts die Anzeige (HTML)
- unten ein Knopf zum Umwandeln

## Aufgabe

- JavaFX Anwendung erstellen
- Layout gestalten
- Umwandlung implementieren
  - eigene Klasse für das Konvertieren
  - Tests

## Details

### Layout (Beschreibung)
- BorderPane als Basis
  - unten ein Button
  - in der Mitte
    - links: Textarea
    - rechts: WebView mit “text/html”-Inhalt
    - diese beiden in ein GridPane verpackt mit je 50 % Breite
- beim Start einfacher, fixer HTML Code im rechten Feld
- es gibt einen Anfang (HTML Header) und ein Ende (HTML Footer) die immer gleich bleiben; gleich sinnvolle Konstanten bzw. Methoden definieren (HTML Grundgerüst)

### Umwandlung
"alle" Formatierungen implementieren
- TiddlyWiki [Tiddlywiki](http://classic.tiddlywiki.com/#Reference) bzw. [Tiddlywiki-Text](https://tiddlywiki.com/#WikiText)
- schwieriger: Mediawiki [Wikipedia Textgestaltung](http://de.wikipedia.org/wiki/Hilfe:Textgestaltung)
- Alternative: Markdown zB. [Pandoc-Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) oder [Wiki Syntax](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
zumindest genug für das Beispiel (siehe unten) sollte funktionieren

### Tipp
- Text in Zeilen auftrennen
- jede Zeile bearbeiten
- HTML Grundgerüst + alle bearbeiteten Zeilen ins Ergebnisfeld schreiben
*meinWebView.getEngine().loadContent(...)*
- zum Testen: Anzeige HTML Quellcode, dazu braucht man nur eine kleine Änderung

### Reihenfolge der Implementierung
Tipp:
- zuerst Überschrift erster Ordnung
- dann Überschrift zweiter Ordnung
  - nachdenken; wie kann man h1 bis h6 implementieren
- dann normaler Text, Leerzeile ist neuer Absatz
- einfache Textersetzungen
  - fett, kursiv, Code eventuell hoch/tiefgestellt
- Links (Anker)
- Trennlinie
- Sonderfall beachten: mehrere (gleiche) Ersetzungen in der Zeile
Ein paar Test wären vielleicht nicht schlecht… – falls noch nicht passiert: eigene Klasse für das Konvertieren + Tests
- etwas aufwändig
  - Listen/Aufzählungen mit * bzw. # am Anfang
  Tipp: man vergleicht die Anzahl der * in dieser Zeile mit der (gespeicherten) Anzahl der * in der vorigen Zeile
und …
- sehr aufwändig (Bonusaufgabe)
  - Tabelle

### Beispieleingabe
```
Der erste
Absatz.

Noch ein Absatz.

normaler Text,
ein ''fetter'' Text.
ein //kursiver// Text -
Text kann ''fett'' oder //kursiv// sein - oder ''//beides//''.

! Header
!! SubHeader

normaler Text

*Liste
**Unterliste
*weiter mit Liste
nach der Liste
!! SubHeader mit
//kursiv// Text
* Liste am Ende
```

# Aufbauend auf der Klasse BirthdayDatamining der CORE Aufgabe und indem du die Klasse Birthday aus der CORE Aufgabe verwendest:

Schreibe das Programm BirthDayDatamining , das die Textdatei [birthdays.txt](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/birthdays.txt) einliest und die unten
beschriebenen Informationen ausgibt.

- Die Klasse soll alle Geburtstage in einer Liste speichern.
  - Die Datei mit den Geburtstagen soll dem Konstruktor angegeben werden können.
- Jede Aufgabe soll mit einer eigenen Methode gelöst werden, die sinnvoll zum Testen aus der main-Methode aufgerufen wird.
- Wähle unbedingt sinnvolle aussagekräftige Namen für alle Methoden, Parameter, Variablen, etc.
- Du kannst gerne beliebig viele Hilfsmethoden schreiben.
- verwende (wenn nötig) passende Collections, keine Arrays !!

## 1. Was ist der Mittelwert aller Jahre (Arithmetisches Mittel).

- der Mittelwert ist: 19??.641975308642
## 2. Welches ist das häufigste Geburtsjahr?

- Tipp: Verwende eine **Map<GeburtsJahr, Anzahl>** zum Zählen.
- das häufigste Jahr ist: 1??6 (das kommt insgesamt 244x vor)
## 3. Ermittle die Anzahl der Geburtstage pro Monat.

- Die Geburtstags-Anzahl soll in einem int-Array stehen.
- 1:??9 2:??2 3:?1 4:??5 5:??0 6:?4 7:?6 8:??0 9:114 10:??5 11:??7 12:??3
## 4. Schreibe eine Methode, die ermittelt wie viele Personen an einem bestimmten Tag Geburtstag haben.

- Das gesuchte Geburtsdatum soll durch zwei int-Parameter (day, month) angegeben werden können.
- Teste mit dem 29. Februar und deinem eigenen Geburtstag.
  - am 22. Mai haben 3 Geburtstag.
  - am 24. Dezember haben 3 Geburtstag.
## 5. Wie viele sind in einem Schaltjahr geboren ?

- in einem Schaltjahr sind geboren: ?80
## 6. Wann hat der Jüngste Geburtstag ?

- der Jüngste hat Geburtstag am: 03.11.????
## 7. Wann hat der Älteste Geburtstag ?

- der Älteste hat Geburtstag am: 14.03.????
## 8. In welchem Jahr hat der jüngste Lehrer Geburtstag?

- Tipp: Der älteste Schüler ist mindestens 4 Jahre jünger als der jüngste Lehrer. Es reicht, wenn du die Jahreszahlen betrachtest.
- Geburtsjahr des jüngsten Lehrers: ???7
## 9. Wie viele Lehrer gibt es?

- es gibt ??4 Lehrer
## 10. Gib mir alle Geburtstage der Lehrer in einer Liste.

- Die Reihenfolge der Geburtstage soll dabei nicht verändert werden.
- der erste und letzte Lehrer-Geburtstag ist: 31.05.1961, 20.04.1958
## 11. Gib mir alle Geburtstage der Schüler in einer Liste.

- Die Reihenfolge der Geburtstage soll dabei nicht verändert werden.
- der erste und letzte Schüler-Geburtstag ist: 05.07.1997, 26.04.1995
## 12. Wie viele Personen teilen sich den gleichen Geburtstag?

- Tipp: Verwende eine Map, um für jeden Tag des Jahres die Anzahl der Geburtstags-Kinder zu bestimmen.
- Anzahl der Personen die sich einen Geburtstag teilen: 1??5

## Collections – „Cocktails“

In einer Bar werden unterschiedliche Cocktails gemixt. Für die Cocktails sind verschiedene Zutaten notwendig.
Leider sind nicht immer alle Zutaten verfügbar, und der Barkeeper muss, bevor er zu arbeiten beginnt,
ermitteln, welche Cocktails mit den vorhandenen Zutaten gemixt werden können. Die Daten für die Cocktails
können aus dem alten Kassensystem nur über Textdateien exportiert werden.
Um den Barkeeper zu unterstützen, erstelle eine Java Applikation, die die Daten der Cocktails und die
vorhandenen Zutaten aus einer [Textdatei](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/CocktailMix.txt) einliest und dann die Liste der möglichen Cocktails sortiert auf
**System.out** ausgibt.

Die [Textdatei](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/CocktailMix.txt) hat dabei folgendes Format:
```
Anzahl der Zutaten
Name der Zutat:Namen der Cocktails mit dieser Zutat
vorhandene Zutaten
```
Beispieldatei:

```
4
lime juice:Caipirinha,Margarita
cachaca:Caipirinha
contreau:Margarita
tequila:Margarita
lime juice, cachaca
```
Ausgabe:
```
Caipirinha
```
Verwende die Java Collections und verwalte die unterschiedlichen Cocktails in einer **Map** und die
Zutaten in einem **Set**.

Cocktail Map
|  Cocktail (Key)   | Zutaten des Cocktails (Value)    |
| -------------    |:-------------:         | 
|    Cocktail..    | ...Set mit Zutaten...  |
|      ...         | ...Set mit Zutaten...  |

[Testdaten](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/CocktailMix.txt):
```
14
pineapple juice:Latin Lover,Carribean Dream,Malibu Barbados,Malibu Margarita
cachaca:Brazilian Sunrise,Latin Lover,Caipirinha,Caipifragola
carribean rum with coconut:Malibu Barbados,Malibu Margarita,Malibu Sunrise
cointreau:Margarita,Malibu Margarita,Daiquiri
grenadine:Brazilian Sunrise,Tequila Sunrise,Carribean Dream,Malibu Sunrise
lemon juice:Brazilian Sunrise,Tequila Sunrise,Latin Lover,Carribean Dream,Malibu Margarita
lime juice:Latin Lover,Caipirinha,Caipifragola,Strawberry Daiquiri,Margarita,Malibu Margarita
orange juice:Brazilian Sunrise,Tequila Sunrise,Carribean Dream,Malibu Barbados,Malibu Sunrise
passion fruit nectar:Carribean Dream
rum:Strawberry Daiquiri,Carribean Dream,Daiquiri
strawberry liqueur:Caipifragola,Strawberry Daiquiri
strawberry sirup:Caipifragola
tequila:Tequila Sunrise,Latin Lover,Margarita,Malibu Margarita
sugar:Caipirinha,Daiquiri
cachaca,lime juice,grenadine,lemon juice,orange juice,carribean rum with coconut,sugar
```
Ausgabe:
```
Brazilian Sunrise
Caipirinha
Malibu Sunrise
```

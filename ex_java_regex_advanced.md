
# Suchen
## 1. Schreibe die Methode
```
Map<String, Double> tagStatistik(Path htmlDatei) { ... }
```
Diese Methode soll im Ergebnis zu jedem HTML-Tag aus der htmlDatei die relative Häufigkeit des
Vorkommens (bezogen auf die Gesamtanzahl der Tags) liefern, d.h. eine Zahl zwischen 0 und 1
- Zwischen Groß- und Klein-schreibung soll nicht unterschieden werden.
## 2. Schreibe die Methode
```
Set <String> links(Path htmlDatei) { ... }
```
Diese Methode soll alle absoluten Links liefern, die in der htmlDatei vorkommen, d.h. Links, die mit
http:// oder https:// beginnen.
## 3. Schreibe die Methode
```
Set<String> domains(Path htmlDatei) { ... }
```
Diese Methode soll die Domains aller absoluten Links liefern, die in der htmlDatei vorkommen.
Die Domain umfasst die beiden letzten Bestandteile des Hostnamens, z.B. ist
```
rennweg.at
```
die Domain von
```
http://www.htl.rennweg.at/index.html
```
## 4. Schreibe die Methode
```
Set<String> klassen(Path cssDatei) { ... }
```
Diese Methode soll alle CSS-Klassen liefern, auf die in der cssDatei Bezug genommen wird, d.h. Namen, die
mit einem Punkt beginnen.
# Suchen und Ersetzen
## 5. Schreibe Methoden, die ihr String-Argument in der folgenden Weise umformen und das Ergebnis als String
liefern:
1. Mehrfache Leerzeichen sollen durch jeweils ein einziges Leerzeichen ersetzt werden.
1. Jedes Wort soll einen großen Anfangsbuchstaben bekommen.
1. Der Text soll in Zeilen aufgeteilt werden, die max. 40 Zeichen lang sind. Die Zeilentrennungen dürfen nur bei
Leerzeichen erfolgen.
# Chemische Gleichungen
## 6. A chemical equation represents a chemical reaction and consists of the chemical formula of the reactants
and the chemical formula of the products. Reactants and products are separated by an arrow symbol ("->").
Individual substances's chemical formula are separate by a plus sign. The chemical formula of a substance can
be preceeded by a number indicating multiple molecules.
Some example for chemical equations are the reaction of hydrochloric acid with sodium:
```
2 HCl + 2 Na -> 2 NaCl + H2
```
or the equation for photosynthesis:
```
6 CO2 + 6 H2O -> C6H12O6 + 6 O2
```
A chemical equation is considered formally correct if the sum of each type of atoms on the left side of the "->"
equals the amounts for each atom type on the right side.
Problem description
Write a program that reads chemical equations from a file [equations.txt](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/equations.txt). For each line with a
chemical equation the program shall output "formally correct" or "incorrect". The formulae can contain any
amount of any element, but there will be no ionized elements or single electrons. Spaces are set as in the
example, but you are encouraged to provide a program that is whitespace-tolerant.
Example Input
```
HCl + Na -> NaCl + H2
2 HCl + 2 Na -> 2 NaCl + H2
12 CO2 + 6 H2O -> 2 C6H12O6 + 12 O2
```
Example Output
```
incorrect
formally correct
incorrect
```

# Chat-Server

### 1) Funktionsbeschreibung

Schreibe einen einfachen Chat-Server, der das Chatten über einen normalen Telnet-Client erlaubt. Der Server stellt einen einzigen Chat-Raum zur Verfügung.

### 2) Ablauf aus Sichts der Clients

Sobald sich der Client mit dem Server verbindet, erscheinen eine Begrüßung sowie die Aufforderung, einen Spitznamen einzugeben. Der Server überprüft, ob schon ein Client mit einem "ähnlichen" Spitznamen angemeldet ist und fordert den Client, falls nötig, zur Eingabe eines anderen Spitznamens auf:

```
Willkommen beim Chat-Server der 3xI
Um die Verbindung zu beenden gib quit ein.
Welchen Spitznamen moechtest du haben: miau
Der Spitzname "miau" ist leider schon vergeben.
Waehle einen anderen: BIS
```

Sobald sich der Client mit einem zulässigen Spitznamen angemeldet hat, wird dieser Spitzname mit einem nachgestellten '>' als Eingabeaufforderung verwendet.

```
BIS>
```

Bei allen anderen Clients erscheint diese Nachricht:

```
"BIS" hat den Raum betreten.
```

An der Eingabeaufforderung kann man nun zeilenweise die Nachrichten eingeben:

```
BIS> Hi Leute, wie geht's euch heute?
```

Sobald man die Enter-Taste drückt, erscheint diese Zeile bei allen anderen Clients in dieser Form:

```
BIS: Hi Leute, wie geht's euch heute?
```

Nachrichten, die man während des Eintippens einer eigenen Nachricht erhält, werden oberhalb der eigenen Nachricht eingefügt, ohne dass die bisher getippte Nachricht verloren geht. (siehe Kapitel Telnet-Steuerzeichen weiter unten)

Um sich vom Chat abzumelden, gibt man das Wort *quit* als Nachrichtentext ein:

```
BIS> quit
```

Daraufhin beendet der Server die Verbinung, und bei allen anderen Clients erscheint die Meldung:

```
"BIS" hat den Raum verlassen.
```

### 3) Ablauf aus Sicht des Servers

Der Server verwaltet in geeigneten Strukturen alle verbundenen Clients (Threads) sowie die "Spitznamen", die die angemeldeten User gewählt haben.

Jede Nachricht, die in einem Client editiert wird, soll an den Server weitergeleitet werden, der dafür verantwortlich ist, diese Nachricht an alle anderen Clients zu verteilen.

Folgende Ereignisse sollen in der Konsole protokolliert werden:
- wenn der Server bereit ist (unter Angabe des Ports)
- wenn sich ein Client zum Server verbindet oder die Verbindung beendet wird (unter Angabe von Client- IP-Adresse und -Port)
- wenn ein Client den Chat-Raum betreten oder verlassen hat (unter Angabe des Spitznamens)
- sonstige Fehler (mit Angabe der Fehlerbeschreibung)

### 4) Tipps

nach dem accept() sollte man ein neues Client/Worker-Objekt anlegen und
- startet sofort einen neuen Thread (damit der eigentliche Server weiter arbeiten
kann)
  - sinnvoll: im Konstruktor einen neuen Thread starten – diese Aufgabe geht den
Server nichts an
- behandelt die Kommunikation mit diesen Clients
  - verwaltet die Clients selbst, den Namen der Clients etc.
- trägt den Client beim Server in die Lister alle Clients ein
  - Überlege wann das sinnvoll ist?
- löscht den Client beim Verlassen bzw. bei Verbindungsabbruch

### 5) Erweiterungen

- Der Client kann mit dem Befehl list die Liste aller angemeldeten Teilnehmer (Nicknames) anfordern.
- Der Client kann mit dem Befehl stat eine Statistik anforden, in der steht, welcher Teilnehmer wie viele Nachrichten verschickt hat.
- Der Client kann eine Nachricht an einen einzelnen Teilnehmer verschicken, ohne dass die anderen Teilehmer sie bekommen. Dazu schreibt er vor die eigentliche Nachricht den Nickname des gewünschten Teilnehmers, dann einen Doppelpunkt und dahinter die eigentliche Nachricht.
- Beispiel: der Teilnehmer "BIS" schickt an den Teilnehmer "HOR" die Nachricht "Ist Deine Gruppe schon fertig?":
```
BIS> HOR: Ist Deine Gruppe schon fertig?
```

### 6) Telnet-Steuerzeichen

Diese Byte-Sequenzen sind jeweils vor und nach einem Strings einzufügen, der an einen (Telnet)-Client gesendet wird.

```
/** ANSI-Terminalsteuerzeichen, die *vor* jeder Nachricht gesendet werden */
//  VT           ggf. Leerzeile am unteren Bilschirmrand (Workaround für Linux, 1/2)
//  ESC [ 1 A    Cursor um eine Zeile hinauf, wo er vorher war (Workaround für Linux, 2/2)
//  ESC 7        Position des Cursors in der Eingabezeile merken
//  ESC [ 1 L    eine Leerzeile für Nachricht einfügen, Eingabezeile rückt hinunter
//  CR           an den Anfang der Leerzeile
private static final String STZ_DAVOR = new String(new byte[] { 0x0b, 0x1b, '[', '1', 'A', 0x1b, '7', 0x1b, '[', '1', 'L', '\r' });

/** ANSI-Terminalsteuerzeichen, die *nach* jeder Nachricht gesendet werden */
//  ESC 8        zurück zu gespeicherter Cursorposition
//  ESC [ 1 B    Cursor um eine Zeile abwärts, bzw. bis zum Bildschirmrand
private static final String STZ_DANACH = new String(new byte[] { 0x1b, '8', 0x1b, '[', '1', 'B' });
```


Du kannst die Files in diesem Zip-Archiv verwenden, beachte aber, dass die Verbindung über Telnet stattfinden soll:
[TCPServer.zip](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/TCPServer.zip)


## Texte überprüfen

Schreibe Methoden, die die folgenden String-Argumente auf Richtigkeit (formal und inhaltlich) überprüfen und
ein boolean-Ergebnis liefern:
Für die formale Überprüfung verwende die String-Funktion .matches(String regex)

- IPv4-Adressen (vier dezimale Oktette)
- MAC-Adressen, herstellerspezifische Kennungen siehe http://standards.ieee.org/develop/regauth/oui/oui.txt
- Dezimalzahlen, die als float-Werte darstellbar sind (Komma- oder Ganzzahlen, auch mit Vorzeichen)
- Datumsangaben wie z.B. 7.11.2013 (die Jahreszahl darf entfallen oder mit zwei Ziffern abgekürzt werden)
- Österreichische KFZ-Kennzeichen (inkl. Wunschkennzeichen), siehe http://de.wikipedia.org/wiki/Kfz-Kennzeichen_(Österreich)
Teste die Methoden, indem du sie mit verschiedenen Parametern aufrufst und die Ergebnisse
ausgibst.

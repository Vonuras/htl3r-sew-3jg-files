## 1 Das Acht-Damenproblem
Es handelt sich hier um ein Schachproblem, das erstmals von M. Bezzel 1845 in einer Schachzeitung veröffentlicht wurde, aber vorerst unbeachtet blieb. Erst als die Aufgabe 1850 vom blinden Schachexperten Dr. Nauk erneut zur Diskussion gestellt wurde, fand sie breites Echo. Als Nauk 3 Monate später alle 92 Lösungen publizierte, hatte der berühmte C.F. Gauss erst 72 Lösungen gefunden!
### Die Aufgabe:
Man finde alle Möglichkeiten, 8 Damen auf einem Schachbrett (8 x 8 Felder) so zu platzieren, dass keine eine andere bedroht. (Eine Dame kann auf allen Feldern waagrecht, senkrecht und diagonal schlagen)
[Erklärung mit Beispielen](https://de.wikipedia.org/wiki/Damenproblem)

Das Programm soll für jede der gefundenen Lösungen die Positionen der Damen ausgeben:
z.B für die symetische Lösung von Wikipedia (Zeilen 1 – 8 ; Spalten A – H):
“Lösung 1 : D8 F7 H6 B5 G4 A3 C2 E1“
### Die erweiterte Aufgabe:
Man finde alle Möglichkeiten, n Damen auf einem n x n großen Spielbrett so zu platzieren, dass keine eine andere bedroht. (Eine Dame kann auf allen Feldern waagrecht, senkrecht und diagonal schlagen)
zu erwartende Ergebnisse findet ihr ebenfalls im Wikipedia Artikel
### Bonusaufgabe (für alle die sich interessieren und es genau wissen oder einfach nur üben wollen):
Lasse dein Programm berechnen wieviele eindeutigen Lösungen es gibt. D.h. ohne Drehungen und Spiegelungen.
## 2 Sudoku
Das Programm soll in der Lage sein:
- Sudokus aus einer Datei zu lesen
- sie zu lösen,
- die Lösung zu speichern
- die Sudokus (in einem für den Menschen sinnvollen Darstellung) anzuzeigen.
- in einem Ordner Lösungen für alle Sudoku-Files erzeugen

### A Sudoku aus einer Datei lesen
Lies eine Datei ein und speichere die Sudoku-Angaben sinnvoll ab.
In einer Datei können mehrere Sudokus stehen. Beispiel die Datei [easy.sudoku](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/easy.sudoku):
```
easy_01: .....71..15....7..39....8.5..2.5...8.6...291......1.6.....2.6......192.........53
easy_02: .12..64...9........4.7..2..1......427.59......6.3.....9745..........3.....6..4.59
easy_03: ........3.5...8.2...84..7.....2...34...6..8...4.3.5.97.62.5......4..3..9...7...5.
```
In jeder Zeile steht genau ein Sudoku.
Zuerst steht der Name des Sudokus, danach das eigentliche Sudoku mit genau 81 Zeichen (den Ziffern 1-9 und dem Punkt)
Ein Punkt steht für ein leeres Feld.
Je neun Zeichen bilden eine Zeile. Die Zeilen sind nicht voneinander getrennt.
Damit ergibt sich für das Sudoku **easy_01**:
```
+---+---+---+
|   |  7|1  |
|15 |   |7  |
|39 |   |8 5|
+---+---+---+
|  2| 5 |  8|
| 6 |  2|91 |
|   |  1| 6 |
+---+---+---+
|   | 2 |6  |
|   | 19|2  |
|   |   | 53|
+---+---+---+
```
### B Löse alle eingelesenen Sudokus
Dateien:
[simple.sudoku](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/simple.sudoku)
[easy.sudoku](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/easy.sudoku)
[intermediate.sudoku](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/intermediate.sudoku)
[expert.sudoku](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/expert.sudoku)
[sudoku.solutions](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/sudoku.solutions)
- Bearbeite nun der Reihe nach alle Sudokus:
  - Gib vor dem Lösen den Namen und das ungelöste Sudoku wie in der vorigen Grafik in die Konsole aus.
  - Löse das Sudoku.
  - Gib die Lösung in die Konsole aus.
- Speichere nun alle Lösungen gemeinsam in der Datei **sudoku.solved**.
  - Dabei sollen die Lösungen an die Datei angehängt werden (append!).
  - Jedes Sudoku steht in einer eigenen Zeile mit dem Format:
  SudokuName;angabe;Lösung
  **Zusätzlich** kannst du auch zusätzlich die Formatierung in die Datei schreiben (kein muss)

## Problem 1
Hier ist eine Klasse die User Input auswerten und die Summe der eingegebenen Zahlen zurückliefern soll.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[Sum.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Sum.java)

## Problem 2
Hier ist eine Klasse für Summe und Durchschnitt eines int-arrays.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[SumAverage.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/SumAverage.java)

## Problem 3
Als kleine Tech-Demo wollte jemand eine Oberfläche erzeugen, die aus 5 Buttons besteht und den Hintergrund der rechten Hälfte der Oberfläche ändert.
Behebe die Probleme im Code, formatiere den Code und versuche ihn zu verbessern.
[Buttons.java](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Buttons.java)


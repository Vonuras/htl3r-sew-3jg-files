## Definiere reguläre Ausdrücke für die formale Überprüfung der folgenden Arten von Text. Teste mit dem RegexpTester - Plugin für IntelliJ von Sergey Evdokimov oder mit https://regex101.com/.

- IPv4-Adressen (vier dezimale Oktette)
- IPv6-Adressen (acht Gruppen von vierstelligen Hexadezimalzahlen)
- MAC-Adressen
- Flugnummern (Airline: 2-3 Großbuchstaben, Flugnummer: 2-4 Ziffern)
- Dezimalzahlen (Komma- oder Ganzzahlen, auch mit Vorzeichen)
- Datumsangaben wie z.B. 7.11.2013 (die Jahreszahl darf entfallen oder mit zwei Ziffern abgekürzt werden)
- Email-Adressen
- Binärzahlen mit einer geraden Anzahl von Einsen
- Binärzahlen, in denen keine Einsen und Nullen unmittelbar aufeinander folgen

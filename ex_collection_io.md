## 1. Schreibe die Methode
```
Set<Integer> lottoZiehungN() { … }
```

Diese Methode soll eine Lottoziehung simulieren und das Ziehungsergebnis (6 aus 45 Zahlen) als Set liefern. Implementiere beide der folgenden Varianten.

Variante1: Erzeuge in der Methode lottoZiehung1() so lange Zufallszahlen aus dem Bereich 1 bis 45, die im Set gespeichert werden, bis die Größe des Sets gleich 6 ist (das Set verhindert, dass Duplikate gespeichert werden!)

Variante2: Die Methode lottoZiehung2() soll genauso ablaufen wie eine Lottoziehung: die erste Zahl wird aus 45 Zahlen gezogen, die zweite aus den restlichen 44 Zahlen, die dritte aus den restlichen 43 Zahlen usw.
In der Ergebnismenge sollen die Zahlen aufsteigend sortiert sein.

## 2. Schreibe die Methode

```
Set<String> vereinigung(Set<String> a, Set<String> b) { … }
```

Diese Methode soll ein neues Set liefern, das aus denjenigen Elementen besteht, die entweder im Set a oder im Set b oder in beiden Sets enthalten sind.

## 3. Schreibe die Methode

```
Set<Integer> durchschnitt(Set<Integer> a, Set<Integer> b) { … }
```

Diese Methode soll ein neues Set liefern, das aus denjenigen Elementen besteht, die sowohl im Set a als auch im Set b enthalten sind.

## 4. Schreibe die Methode

```
Set<Double> differenz(Set<Double> a, Set<Double> b) { … }
```

Diese Methode soll ein neues Set liefern, das aus denjenigen Elementen besteht, die zwar im Set a, nicht aber im Set b enthalten sind.

## 5. Schreibe die Methode

```
Set<String> unikate(List<String> a) { … }
```

Diese Methode soll ein neues Set liefern, das aus denjenigen Elementen besteht, die in der List a genau einmal vorkommen.

## 6. Schreibe die Methode

```
void copyWinners(String srcFileName, String destFileName),
```

In der Datei [lotto.dat](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/lotto.dat) stehen die Lotto-Zahlen der Ziehung am Anfang und danach noch 1.000 Lotto-Tipps.
Die Lotto-Zahlen der Ziehung bzw. die Tipps stehen in jeweils 6 Bytes hintereinander, jede Zahl in einem eigenen Byte.

In dieser Methode sollen alle Lotto-Gewinner (d.h. alle Tipps, die mindestens drei Richtige haben) aus der Datei srcFileName in die csv-Datei destFileName kopiert werden. Dabei sollen am Anfang der Datei wieder die Lotto-Zahlen der Ziehung und danach die Lotto-Gewinner stehen. Die 6 Zahlen der Ziehung bzw. der Tipps sollen aufsteigend sortiert und durch einen Strichpunkt voneinander getrennt geschrieben werden.
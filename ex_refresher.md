### Allgemeine Hinweise

- Das abgegebene Projekt muss lauffähig sein.
- Achte auf eine korrekte Formatierung deines Codes.
- print/println stehen nur in der main-Methode.
- Alle Variablen und Methoden müssen sinnvolle Namen haben.

### 1. Klassenhierachie & File I/O mit Textdateien
Schreibe ein Programm zur Verwaltung von Charakteren in einem Computerspiel.

## 1.1. Klassen
Es sind drei Klassen zu schreiben: GameCharacter, Warrior und Farmer.

- Von der Klasse GameCharacter können keine Objekte erzeugt werden.
- Die Klassen Warrior und Farmer leiten sich von der Klasse GameCharacter ab.
- Die Klassen besitzen folgende Attribute:
  - GameCharacter: name (String), clan (String), energy (int)
  - Warrior: weapon (String), force (int)
  - Farmer: product(String), productivity (int)
- Implementiere für jede der drei obigen Klassen einen Konstruktor, um alle Attribute zu setzen.
- Ergänze bei allen Klassen eine sinnvolle toString-Methode, die alle Attribute ausgibt. (Tipp: Mittels getClass().getSimpleName() ermittelst du den Namen der Klasse).
  - Beispiele:
    - Warrior: Name: Pyrrhos Clan: Gummibärenbande Energy: 90 Weapon: Sword Force: 240
    - Farmer: Name: Henry Clan: Gummibärenbande Energy: 45 Product: Corn Productivity: 120
  - Achte darauf, dass du keinen Code mehrmals schreiben musst, nutze unbedingt die Möglichkeiten der Vererbung.
- Implementiere setter-Methoden und getter-Methoden für energy, force und productivity in der jeweils passenden Klasse.
- Stelle sicher, dass jede Unterklasse von GameCharacter die Methode String getSlogan() implementieren muss. Implementiere in jeder Unterklasse diese Methode und lass sie den Default-Slogan des jeweiligen Charakters zurückliefern (frei von dir wählbar).

## 1.2. File I/O
Schreibe die Klasse CharacterTest, die folgende Aufgaben durchführt:

- Die Klasse CharacterTest besitzt das Attribut List<GameCharacter> characters, um Charaktere zu speichern.
  Hinweis: Sollte die vorherige Aufgabe gar nicht zufriedenstellend entwickelt worden sein, dann speichere in der ArrayList direkt die csv-Zeile.
- Schreibe die Methode void loadTextFile(String textFile), die aus dem csv-File textFile alle Charaktere lädt und in der Liste characters speichert.
  - Alle alten Charaktere, die schon in der ArrayList characters gespeichert sind, sollen vor dem Laden gelöscht werden.
  - Die Datei ist mit dem Zeichensatz »utf-8« codiert.
- Schreibe die Methode void savePoints(String textFile), die folgende Werte ermittelt:
  - Summe der energy aller Charaktere.
  - Summe der force aller Warrior.
  - Summe der productivity aller Farmer.
- Diese drei Werte sollen durch einen Bindestrich (“-”) getrennt in die Datei textFile geschrieben werden.
- Die Klasse CharacterTest muss zum Testen eine main-Methode haben. In der main-Methode muss jede Methode mindestens einmal sinnvoll getestet werden.

Dateiauszug [characters.csv](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/characters.csv):
```
warrior;Pyrrhos;Gummibärenbande;90;Sword;240
farmer;Henry;Gummibärenbande;45;Corn;120
Die Attribute eines Warriors sind in folgender Reihenfolge gespeichert:
[name];[clan];[energy];[weapon];[force]
Die Attribute eines Farmers sind in folgender Reihenfolge gespeichert:
[name];[clan];[energy];[product];[productivity]
```

### 2. Binär zu CSV
In der beigelegten binären Datei [people.dat](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/people.dat) sind Personendaten binär gespeichert. In jeweils 80 Bytes stehen
die Personalnummer, Vorname, Nachname und die E-Mail Adresse (alle UTF-8 kodiert) einer Person.

- Datenformat der Datei:
  - Byte 0-9: Personalnummer
  - Byte 10-29: Vorname
  - Byte 30-49: Nachname
  - Byte 50-79: E-Mail

**Schreibe die Methode:**
*void convertBin2CSV(String oldFilename, String newFileName) throws IOException* die die Byte-Datei (oldFileName) in eine moderne csv-Datei (durch Strichpunkt getrennte Felder, UTF-8 codiert) umwandelt.
Ausschnitt binäre Datei (nicht verwendete Zeichen sind als Bytes mit dem Wert 0 im Eingabefile gespeichert):
```
1NULLNULLNULLNULLNULLNULLNULLNULLNULLAntonioNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLBartonNULLNULLNULL
```
Ausschnitt finale csv-Datei:
```
1;Antonio;Barton;ututuggip@zov.ag
```

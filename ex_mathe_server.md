# Mathe-Server

Verwende die Files in dem Zip-Archiv:
[TCPServer.zip](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/TCPServer.zip)

Ergänze eines der drei Server Files im Zip-Archiv und den ClientHandler so, dass ein Verbindungsaufbau möglich ist.

## 1) Schritt 1: (Grundstruktur)
Wenn sich ein Client verbindet, soll eine zufällig generierte mathematische Aufgabe (Addition,
Subtraktion, Multiplikation, Division) übermittelt werden, das gesendete Ergebnis des Clients
überprüft und mit "richtig/falsch" bewertet werden.

```
Willkommen im Mathe-Quiz
5 + 4 = ??
9
richtig ;)

Verbindung zu Host verloren.
```
Die Rechnung soll zufällig eine der erwähnten vier Grundrechnungsarten sein; die verwendeten
Zahlen sollen natürlich auch zufällig ermittelt werden -> daher muss der Server die erzeugte
Rechnung auch selbst evaluieren, um das gelieferte Ergebnis überprüfen zu können.
Divisionen sollen klarerweise nur ganzzahlig sein. Trick: erzeuge eine Multiplikation (a*b = prod),
Aufgabe ist "Produkt / Multiplikator1(a)", der zweite Multiplikator(b) ist dann das erwartete
Divisionsergebnis.

## 2) Schritt 2: Erweiterungen:
- Antwort darf wiederholt werden, wenn sie falsch war
```
Willkommen im Mathe-Quiz
5 + 4 = ??
8
falsch :( .. versuche es noch einmal
9
richtig ;)

Verbindung zu Host verloren.
```
- Der Client(User) wählt zu Beginn, wie viele Rechnungen er lösen möchte
```
Willkommen im Mathe-Quiz
Wie viele Rechnungen willst du loesen?
3
Frage 1: 5 + 4 = ??

```
- Festlegen eines Schwierigkeitslevels durch Frage an den Client
  - Level 1: kleine Zahlen (5+3 ; 7-2 ; ...)
  - Level 2: zweistellig (~ Ergebniszahlenraum 100)
  - Level 3: wahnwitzig ...
```
Willkommen im Mathe-Quiz
Wie heftig haettest du es denn gerne ? (1-3)
1
Wie viele Rechnungen willst du loesen?
3
(Level 1) Frage 1: 5 + 4 = ??

```



import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Klasse die ein Perderennen simuliert
 */
public class Pferderennen_Template extends Application {


    public static void main(String[] args) {
        Application.launch(args);
    }


    private VBox bars = new VBox();


    private TextArea output = new TextArea("");


    @Override
    public void start(Stage stage) throws Exception {

        stage.setResizable(false);
        stage.setTitle("Ascot live");
        BorderPane all = new BorderPane();
        Label name = new Label("Pferderennen mit Threads");
        Button btplus = new Button("+");
        Button btminus = new Button("-");
        VBox buttons = new VBox(btplus, btminus);
        Button btstart = new Button("Start");
        all.setTop(name);

        all.setLeft(buttons);

        all.setCenter(bars);

        all.setRight(btstart);

        all.setBottom(output);

        //Example code für ProgressBar handling .. kann so nicht verwendet werden --> Thread !!!
        ProgressBar example = new ProgressBar();
        example.setProgress(0.001);
        example.setMaxWidth(Double.MAX_VALUE);

        double progress = example.getProgress();
        System.out.println(progress);

        bars.getChildren().add(example);

        example.setProgress(0.75);


        output.appendText("Hier wird Text ausgegeben ....\n");

        btplus.setOnAction(event -> {
            //your code here
        });


        btminus.setOnAction(event -> {
            //your code here
        });

        btstart.setOnAction(event -> {
            //your code here
        });

        Scene scene = new Scene(all, 900, 700);
        stage.setScene(scene);
        stage.show();

    }
}
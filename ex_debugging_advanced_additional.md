## Debugging

Ein(e) Bekannte(r) von dir studiert gerade Molekulare Biotechnologie und braucht deine Hilfe.

Das Programm, dass er/sie für einen Kurs schreiben muss will einfach nicht funktionieren.

Kannst du ihr/ihm helfen, dass Programm zum Laufen zu bringen und das richtige Ergebnis zu liefern ?

[Analysis.R](https://gitlab.com/Vonuras/htl3r-sew-3jg-files/-/blob/main/data_files/Analysis.R)


Zum ausführen des Programms kannst du folgenden Online-Compiler verwenden:
[Compiler](https://www.programiz.com/r/online-compiler/)

Der Output des Programms sollte folgende Werte ausgeben:
```
[1] "step 1"
[1] 0.0005601577
[1] 1.455465
[1] -2.1866474 -0.7242823
attr(,"conf.level")
[1] 0.95
[1] "step 2"
[1] 0.572
[1] 0.534 0.560 0.566 0.562 0.554 0.534 0.552 0.568 0.584 0.556
[1] 0.5583 0.5672 0.5675 0.5649 0.5648 0.5663 0.5600 0.5568 0.5614 0.5657
[1] "step 3"
[1] 0.548 0.674 0.700 0.736 0.772 0.814 0.806 0.882 0.888 0.902 0.928 0.928
[13] 0.956 0.964 0.968 0.974 0.974 0.976 0.994 0.984 0.988 0.992 0.992 0.984
[25] 0.992 0.998 1.000 1.000 1.000 0.996 0.998 0.996 1.000 0.998 1.000 1.000
[37] 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000
[49] 1.000 1.000 1.000
[1] 0.512 0.580 0.662 0.672 0.758 0.710 0.798 0.806 0.834 0.832 0.850 0.870
[13] 0.848 0.852 0.884 0.880 0.868 0.882 0.862 0.832 0.866 0.856 0.816 0.774
[25] 0.766 0.766 0.716 0.690 0.634 0.598 0.536
[1] 0.564 0.620 0.724 0.776 0.858 0.886 0.918 0.944 0.972 0.988 0.992 0.994
[13] 0.996 0.996 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000 1.000
[25] 1.000 1.000 1.000 1.000 1.000 1.000 1.000
[1] "program done"

```

